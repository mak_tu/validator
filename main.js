function validate(data, fields) {
    return new Promise((resolve, reject) => {
        if (!fields || !(fields instanceof Array)) reject(new Error('Incorrect argument fields'));
        let result = true;
        if (data instanceof Array) {
            for (const elem of data) {
                const propArray = Object.keys(elem);
                if (propArray.length !== 2 || fields.indexOf(elem[propArray[0]]) === -1 || 
                    typeof elem[propArray[1]] !== 'number' || elem[propArray[1]] !== 'value') {
                    result = false;
                    break;
                };
            };
        } else {
            const propArray = Object.keys(data);
            if (propArray.length !== fields.length) {
                result = false;
            } else {
                for (const prop of propArray) { 
                    const i = fields.indexOf(prop);
                    if (!data[prop].min || !data[prop].max || typeof data[prop].max !== 'number' ||
                        typeof data[prop].min !== 'number' || data[prop].max < data[prop].min || i === -1) {
                        result = false;
                        break;
                    } else {
                        fields.splice(i, 1);
                    } 
                }
            }
        }

        resolve(result);
    });
};

module.exports = validate;